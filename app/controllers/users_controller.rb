class UsersController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  def edit
  end
  
  def create
    data = JSON.parse(params["user"])
    @user = User.new()
    @user.username = data["username"]
    @user.email = data["email"]
    @user.password = data["password"]
    if @user.save
        render json: { response: "success", message: "Użytkownik został zarejestrowany", data: @user }
    else
        render json: { response: "failed", message: "Wystąpił błąd podczas rejestracji użytkownika, proszę spróbować później" }
    end
    
  end

  def index
    @users = User.all
  end

  def show
  end
  
  def login
    data = JSON.parse params['user']
    @user = User.find_by_username data["username"]
    if @user.nil?
      render json: { response: "failed", message: "Użytkownik o podanym loginie nie istnieje", type: 'danger' }
    elsif @user.password != data["password"]
      render json: { response: "failed", message: "Niepoprawne hasło dla loginu #{@user.username}", type: 'danger'}
    else
       render json: { response: "success", data: @user }
    end 
  end
  
  private
  
  def user_params 
    params.require(:user).permit(:username, :email, :password)
  end
end
