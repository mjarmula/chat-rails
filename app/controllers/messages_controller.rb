class MessagesController < ApplicationController
skip_before_action :verify_authenticity_token

  def create
  # data = JSON.parse(params["msg"])
    @msg = Message.new()
    @msg.author = params[:author]
    @msg.body = params[:body]
    @msg.save    
  end    
  
  def index
    @messages = Message.all
    render json: @messages
  end
end
